import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;


public class Operations {
	List<MonitoredData> monitoredData;
	
	public Operations(){
		monitoredData= new ArrayList<MonitoredData>();
		String fisName= "Activities.txt";
		
		try (Stream<String> stream = Files.lines(Paths.get(fisName))) {
			stream.forEach(i-> {String[] start = i.split("\t");
								String[] end = i.substring(21).split("\t");
								String[] activity = i.substring(42).split(" ");
			
			MonitoredData m = new MonitoredData(activity[0].trim(),start[0].trim(),end[0].trim());
			monitoredData.add(m);});

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	//task 1
	public void countDays(){
		List<String> days= monitoredData.stream().map(md -> md.getDay(md.getStartTime())).collect(Collectors.toList());
		
		try{
		    PrintWriter writer = new PrintWriter("Activity1.txt", "UTF-8");
		    writer.println("Number of days: "+ days.stream().distinct().count());
		    writer.close();
		} catch (IOException e) {
		   System.out.println("No file!");
		}
	}
	
	//tack 2
	public void nrOfOccurences(){
		List<String> occurences= monitoredData.stream().map(md->md.getActivityLabel()).distinct().collect(Collectors.toList());
		Map<String,Integer> occurMap = new HashMap<>();
		
		occurences.stream().forEach(o->occurMap.put(o,(int) monitoredData.stream().filter(m->m.getActivityLabel().equals(o)).count()));
		
		try{
		    PrintWriter writer = new PrintWriter("Activity2.txt", "UTF-8");
		    for(String i: occurMap.keySet())
	    		writer.println(i+"->" +occurMap.get(i));
		    writer.close();
		} catch (IOException e) {
		   System.out.println("No file!");
		}
	}
	
	//task 3
	public static Map<String, Integer> nrActivities(String day, Operations op){
		List<String> occurences= op.monitoredData.stream().filter(m-> m.getDay(m.getStartTime()).equals(day)).map(md->md.getActivityLabel()).distinct().collect(Collectors.toList());
		Map<String,Integer> occurMap = new HashMap<>();
		
		occurences.stream().forEach(o->occurMap.put(o,(int) op.monitoredData.stream().filter(m->m.getActivityLabel().equals(o) && m.getDay(m.getStartTime()).equals(day)).count()));
		
		return occurMap;
	}
	
	public void nrOfOccurencesPerDay(){
		List<String> days= monitoredData.stream().map(md -> md.getDay(md.getStartTime())).collect(Collectors.toList());
		Map<String, Map<String,Integer>> occurMap= new HashMap<String, Map<String,Integer>>();
		
		days.stream().forEach(d-> occurMap.put(d,Operations.nrActivities(d, this)));
		
		try{
		    PrintWriter writer = new PrintWriter("Activity3.txt", "UTF-8");
		    for(String i: occurMap.keySet())
		    		writer.println(i+"->" +occurMap.get(i));
		    writer.close();
		} catch (IOException e) {
		   System.out.println("No file!");
		}
	}
	
	//task 4
	public static int totalTime(String activity,Operations o){
		
		int d= o.monitoredData.stream().filter(m-> m.getActivityLabel().equals(activity))
									   .reduce(0,(sum,a)->sum+=a.getActivityHours(),(sum1,sum2)->sum1+sum2);
		return d;
	}
	
	public void activitiesWithHighDuration(){
		Map<String,Integer> durationMap = new HashMap<>();
		
		monitoredData.stream().filter( m -> Operations.totalTime(m.getActivityLabel(), this)>10)
		  					  .forEach(o -> durationMap.put(o.getActivityLabel(),Operations.totalTime(o.getActivityLabel(), this)));
		
		try{
		    PrintWriter writer = new PrintWriter("Activity4.txt", "UTF-8");
		    for(String i: durationMap.keySet())
	    		writer.println(i+"->" +durationMap.get(i));
		    writer.close();
		} catch (IOException e) {
		   System.out.println("No file!");
		}
	}
	
	//task 5
	public static int procentage(String activity, Operations o){
		int lessThan5= (int)o.monitoredData.stream().filter(m-> m.getActivityLabel().equals(activity) && m.getActivityMinutes()<5).count();
		int moreThan5= (int)o.monitoredData.stream().filter(m-> m.getActivityLabel().equals(activity) && m.getActivityMinutes()>5).count();
		
		return lessThan5*100/(lessThan5+moreThan5);	
	}
	
	public void shortActivities(){
		List<String> activities = monitoredData.stream().filter(m-> Operations.procentage(m.getActivityLabel(),this)>90)
														.map(m-> m.getActivityLabel())
														.distinct().collect(Collectors.toList());
		
		try{
		    PrintWriter writer = new PrintWriter("Activity5.txt", "UTF-8");
		    writer.println(activities);
		    writer.close();
		} catch (IOException e) {
		   System.out.println("No file!");
		}
	}
	
	public static void main (String[] args){
		Operations o= new Operations();
		
		o.countDays();
		o.nrOfOccurences();
		o.activitiesWithHighDuration();
		o.shortActivities();
		o.nrOfOccurencesPerDay(); 
	}
}
