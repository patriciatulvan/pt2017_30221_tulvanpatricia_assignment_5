import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class MonitoredData{
	private String activityLabel;
	private String startTime;
	private String endTime;
	
	public MonitoredData(String activityLabel, String startTime, String endTime){
		this.activityLabel=activityLabel;
		this.startTime=startTime;
		this.endTime=endTime;
	}
	
	public String getActivityLabel() {
		return activityLabel;
	}
	
	public void setActivityLabel(String activityLabel) {
		this.activityLabel = activityLabel;
	}
	
	public String getStartTime() {
		return startTime;
	}
	
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	
	public String getEndTime() {
		return endTime;
	}
	
	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
	public String getDay(String time){
		String day = time.split(" ")[0];
		return day;
	}
	
	private long activityTotalTime(){
		try {
			String start = getStartTime();
			String end = getEndTime();
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date startTime;
			Date endTime;
			startTime = dateFormat.parse(start);
			endTime = dateFormat.parse(end);
			long totalTime = endTime.getTime() - startTime.getTime();
			
			return totalTime;
		}catch(ParseException e) {
			
			e.printStackTrace();
		}
		return 0;
	}
	
	public int getActivityHours(){
		return (int) TimeUnit.MILLISECONDS.toHours(activityTotalTime());
	}
	
	public int getActivityMinutes(){
		return (int) TimeUnit.MILLISECONDS.toMinutes(activityTotalTime());
	}

}
